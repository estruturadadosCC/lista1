package q6

class ContaPoupanca(num: Integer, s: Double, taxa: Double) extends ContaBancaria(num, s) {

  protected var juros = taxa

  def renderjuros(juros: Double) {
    saldo = saldo + (saldo * juros)
  }

  override def toString() = "numero = " + numero + ", saldo = " + saldo + ", taxa = " + juros +  "\n"

}