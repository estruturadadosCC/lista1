package q6

class ContaBancaria(num: Integer, s: Double) extends Ordered[ContaBancaria] {

  protected var numero = num
  protected var saldo = s

  def efetuarcredito(valor: Double) {
    saldo = saldo + valor
  }

  def efetuardebido(valor: Double) {
    saldo = saldo - valor
  }
  
  def transferencia(conta: ContaBancaria, valor: Double){   
    efetuardebido(valor)
    conta.efetuarcredito(valor)
    
  }
  
  def consultarSaldo()=saldo

  override def compare(that: ContaBancaria): Int = {
    return this.numero - that.numero
  }
  
  override def toString()="numero = " + numero + ", saldo = " + saldo + "\n"
  
}