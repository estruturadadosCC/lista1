package q6

object Principal6 extends App {
  
  var conta1 = new ContaBancaria(1, 10)
  var conta2 = new ContaBancaria(2, 20)
  
  
  var lista = new ListaEncadeadaOrdenada
  lista = lista.inserirInicio(conta1)
  lista = lista.inserirInicio(conta2)
  
  
  var conta3 = new ContaPoupanca(3, 50, 0.1)
  lista = lista.inserirInicio(conta3)
  
  
  var conta6 = new ContaFidelidade(6, 100)
  lista = lista.inserirInicio(conta6)
  
  println("Todas as Contas:")
  lista.imprimirLista()
  
  conta1.efetuarcredito(30)
  
  println("Todas as Contas:")
  lista.imprimirLista()
  
  conta2.efetuardebido(15)
  println("Todas as Contas:")
  lista.imprimirLista()
  
  println("Saldo da conta 1:")
  println(conta1.consultarSaldo())
  
  println("Bonus da conta fidelidade 6:")
  println(conta6.mostrarbonus())
  
  conta3.renderjuros(0.2)
  
  println(conta6)
  conta6.efetuarcredito(10)
  println(conta6)
  conta6.renderbonus()
  println(conta6)
  
  println("Transferencia:")
  conta1.transferencia(conta2, 10)
  
  println("Todas as Contas:")
  lista.imprimirLista()
  
 
  
}