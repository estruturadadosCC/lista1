package q6

class ContaFidelidade(num: Integer, s: Double) extends ContaBancaria(num, s) {

  protected var bonus: Double = 0

  def renderbonus() {
    saldo = saldo + bonus
    bonus = 0
  }
  
  def mostrarbonus() = bonus

  override def efetuarcredito(valor:Double) {
    var rendimento = valor / 100
    bonus = bonus + rendimento
    saldo = saldo + valor
  }
  
   override def toString() = "numero = " + numero + ", saldo = " + saldo + ", bonus = " + bonus +  "\n"
  
  
}