package q6

class ListaEncadeadaOrdenada {
  //1
  var num: ContaBancaria = null
  var ptr: ListaEncadeadaOrdenada = null

  def inserirInicio(valor: ContaBancaria): ListaEncadeadaOrdenada = { //2
    if (num == null) {
      num = valor
      return this
    } else {

      var l1: ListaEncadeadaOrdenada = this

      while (l1 != null) {
        if (valor < l1.num) {
          var L = new ListaEncadeadaOrdenada
          L.num = valor
          L.ptr = l1
          return L

        } else {
          if (valor >= l1.num && (l1.ptr == null || valor <= l1.ptr.num)) {
            var L2 = new ListaEncadeadaOrdenada
            L2.num = valor
            L2.ptr = l1.ptr
            l1.ptr = L2

            return this
          }
          l1 = l1.ptr
        }
      }
      return this
    }
  }

  def imprimirLista() { //3
    var aux: ListaEncadeadaOrdenada = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      while (aux != null) {
        print(aux.num)
        aux = aux.ptr
      }
    }
  }

  def imprimirRecursivo() { //4
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      print(num)
      if (ptr != null) {
        ptr.imprimirRecursivo()
      }
    }
  }

  def imprimirOrdemReversa() { //5
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      if (ptr != null) {
        ptr.imprimirOrdemReversa()
      }
      print(num)
    }
  }

  def listaVazia(): Int = { //6
    if (num == null) {
      return 1;
    }
    return 0;
  }
  def recuperarElem(indice: Int): ContaBancaria = { //7.1
    var cont: Int = 0
    var aux: ListaEncadeadaOrdenada = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return null
    } else {
      while (aux != null) {
        if (cont == indice) {
          return aux.num
        }
        aux = aux.ptr
        cont = cont + 1
      }
      return null
    }
  }

  def buscarElem(valor: ContaBancaria): Int = { //7.2
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      if (num == valor) {
        return 0
      } else {
        if (ptr == null) {
          print("Nao Existe")
          return -1
        }
        var aux = ptr.buscarElem(valor)
        if (aux == -1) {
          return -1
        }
        return 1 + aux
      }
    }
  }

  def removerElemento(valor: ContaBancaria): ListaEncadeadaOrdenada = { //8

    var l1: ListaEncadeadaOrdenada = this
    var ant: ListaEncadeadaOrdenada = null
    var prime: ListaEncadeadaOrdenada = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {

      while (l1 != null) {
        if (l1.num == valor) {
          if (ant == null) {
            l1 = l1.ptr
            prime = l1
          } else {
            ant.ptr = l1.ptr
            l1 = ant
          }
        }
        ant = l1
        l1 = l1.ptr
      }
      return prime
    }
  }

  def liberarLista(): ListaEncadeadaOrdenada = { //10
    var l1: ListaEncadeadaOrdenada = this
    var ant: ListaEncadeadaOrdenada = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {

      while (l1 != null) {
        //l1.num = null
        //ant.ptr = l1.ptr
        l1 = l1.ptr
        //ant.ptr = null
      }
      return l1
    }
  }

  def verificarIgualdade(l: ListaEncadeadaOrdenada): Boolean = { //11
    var l1: ListaEncadeadaOrdenada = this
    var l2: ListaEncadeadaOrdenada = l

    while (l1 != null) {
      if (l1.num == l2.num) {
        l1 = l1.ptr
        l2 = l2.ptr
      } else {
        return false
      }
    }
    return true
  }

  override def toString() = {
    def ff(): String = {
      var s: String = ""
      var l: ListaEncadeadaOrdenada = this

      while (l.ptr != null) {
        s += l.num
        s += " -> "
        l = l.ptr
      }
      s += l.num
      return s
    }
    ff()
  }
}