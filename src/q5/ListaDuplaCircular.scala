package q5

class ListaDuplaCircular {

  //1
  var num: Integer = null
  var ant: ListaDuplaCircular = null
  var prox: ListaDuplaCircular = null
  var last: Boolean = false

  def inserirInicio(valor: Int): ListaDuplaCircular = { //2
    var L: ListaDuplaCircular = this

    if (L.num == null) {
      L.num = valor
      L.prox = this
      L.last = true
      return this
    } else {
      while (L.last == false) {
        if (valor < L.num) {
          var L2 = new ListaDuplaCircular
          L2.num = valor
          L2.prox = L
          L2.last = false
          return L2
        }
        if (valor >= L.num && valor <= L.prox.num) {
          var L2 = new ListaDuplaCircular
          L2.num = valor
          L2.prox = L.prox
          L.prox = L2
          L2.last = false
          return this
        }
        L = L.prox
      }
      if (valor > L.num) {
        var L2 = new ListaDuplaCircular
        L2.num = valor
        L2.prox = this
        L2.last = true
        L.last = false
        L.prox = L2
        return this
      }
      return this
    }
  }

  def imprimirLista() { //3
    var aux: ListaDuplaCircular = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      while (aux.last == false) {
        print(aux.num)
        aux = aux.prox
      }
      print(aux.num)
    }
  }

  def imprimirRecursivo() { //4
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      print(num)
      if (last == false) {
        prox.imprimirRecursivo()
      }
    }
  }

  def listaVazia(): Int = { //5
    if (num == null) {
      return 1;
    }
    return 0;
  }

  def recuperarElem(indice: Int): Int = { //6.1
    var cont: Int = 0
    var aux: ListaDuplaCircular = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      while (last == false) {
        if (cont == indice) {
          return aux.num
        }
        aux = aux.prox
        cont = cont + 1
      }
      return -1
    }
  }

  def buscarElem(valor: Int): Int = { //6.2
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      if (num == valor) {
        return 0
      } else {
        if (prox == null) {
          print("Nao Existe")
          return -1
        }
        var aux = prox.buscarElem(valor)
        if (aux == -1) {
          return -1
        }
        return 1 + aux
      }
    }
  }

  def removerElemento(valor: Int): ListaDuplaCircular = { //7

    var L: ListaDuplaCircular = this
    var first: ListaDuplaCircular = this
    var ant: ListaDuplaCircular = null

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      while (L.last == false) {
        if (L.num == valor) {
          if (L == first) {
            ant = null
            first = L.prox
            L.num = null
          } else {
            ant.prox = L.prox
            L.num = null
            L = ant
          }
        }
        ant = L
        L = L.prox
      }
      if (L.num == valor) {
        ant.prox = first
        ant.last = true
        L.num = null
      } else {
        if (L.prox == null) {
          L.prox = first
        }
      }
      return first
    }
  }

  def removerElementoRecursivo(valor: Int): ListaDuplaCircular = { //8
    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      if (valor == num) {
        return this.prox
      } else {
        if (prox != null) {
          var aux = prox.removerElementoRecursivo(valor)
          prox = aux
          return this
        } else {
          return this
        }
      }
    }
  }

  def liberarLista(): ListaDuplaCircular = { //9
    var l1: ListaDuplaCircular = this
    var ListaDuplaCircularar = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {

      while (l1.last == false) {
        l1.num = null
        //ant.ptr = l1.ptr
        l1 = l1.prox
        //ant.ptr = null
      }
      l1.num = null
      l1.prox = null
      return l1
    }
  }

  def printListCircular() {
    var L: ListaDuplaCircular = this

    if (L.listaVazia() == 1) {
      printf("Lista Vazia")
    } else {
      println("Mostrando elementos: ")
      while (L.last == false) {
        println("Elemento: " + L.num)
        println("Proximo: " + L.prox.num)
        println("Bool : " + L.last)
        println
        L = L.prox
      }
      println("Elemento: " + L.num)
      println("Proximo: " + L.prox.num)
      println("Bool : " + L.last)
      println

    }
  }

  override def toString() = {
    def ff(): String = {
      var s: String = ""
      var l: ListaDuplaCircular = this

      while (l.last == false) {
        s += l.num
        s += " -> "
        l = l.prox
      }
      s += l.num
      return s
    }
    ff()
  }

}