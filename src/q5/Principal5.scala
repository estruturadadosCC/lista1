package q5

object Principal5 extends App {

  var L = new ListaDuplaCircular

  print("Inserindo valores na lista...\n")

  L = L.inserirInicio(2)
  L = L.inserirInicio(3)
  L = L.inserirInicio(3)
  L = L.inserirInicio(4)
  L = L.inserirInicio(5)
  L = L.inserirInicio(6)

  print("Imprimindo lista...\n")
  L.imprimirLista()
  println

  print("Imprimindo lista usando recursão...\n")
  L.imprimirRecursivo()
  println

  print("Verificar se lista vazia:\n1-Lista Vazia 0-Lista Nao Vazia: \n")
  print(L.listaVazia())
  println

  print("Recuperar o elemento que esta na posicao 2...\n")
  print("O elemento da posicao 2 eh: " + L.recuperarElem(2))
  println

  print("Buscar o elemento 3 na lista...\n")
  print("O elemento 3 esta na posicao: " + L.buscarElem(3))
  println

  print(L)
  println
  print("Remover o elemento 3 da lista. \n")
  L = L.removerElemento(3)
  print(L)
  println
  print("Remover o elemento 5 da lista. \n")
  L = L.removerElementoRecursivo(5)
  print(L)
  println

  println(L)
  print("Verificando lista...\n")
  L.printListCircular()

  println("Removendo Elementos")
  L = L.removerElemento(3)
  println(L)
  L = L.removerElemento(2)
  println(L)
  L = L.removerElemento(6)
  println(L)
  println
  L.printListCircular()

  print("Esvaziar lista...\n")
  L = L.liberarLista()
  println(L)

  L = L.inserirInicio(2)
  L = L.inserirInicio(2)
  L = L.inserirInicio(3)
  L = L.inserirInicio(3)

}