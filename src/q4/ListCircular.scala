package q4

class ListCircular {
  //1
  var num: Integer = null
  var next: ListCircular = null
  var last: Boolean = false

  def inserirInicio(valor: Int): ListCircular = { //2
    var L: ListCircular = this

    if (L.num == null) {
      L.num = valor
      L.next = this
      L.last = true
      return this
    } else {
      while (L.last == false) {
        if (valor < L.num) {
          var L2 = new ListCircular
          L2.num = valor
          L2.next = L
          L2.last = false
          return L2
        }
        if (valor >= L.num && valor <= L.next.num) {
          var L2 = new ListCircular
          L2.num = valor
          L2.next = L.next
          L.next = L2
          L2.last = false
          return this
        }
        L = L.next
      }
      if (valor > L.num) {
        var L2 = new ListCircular
        L2.num = valor
        L2.next = this
        L2.last = true
        L.last = false
        L.next = L2
        return this
      }
      return this
    }
  }

  def imprimirLista() { //3
    var aux: ListCircular = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      while (aux.last == false) {
        print(aux.num)
        aux = aux.next
      }
      print(aux.num)
    }
  }

  def imprimirRecursivo() { //4
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      print(num)
      if (last == false) {
        next.imprimirRecursivo()
      }
    }
  }

  def listaVazia(): Int = { //5
    if (num == null) {
      return 1;
    }
    return 0;
  }

  def recuperarElem(indice: Int): Int = { //6.1
    var cont: Int = 0
    var aux: ListCircular = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      while (last == false) {
        if (cont == indice) {
          return aux.num
        }
        aux = aux.next
        cont = cont + 1
      }
      return -1
    }
  }

  def buscarElem(valor: Int): Int = { //6.2
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      if (num == valor) {
        return 0
      } else {
        if (next == null) {
          print("Nao Existe")
          return -1
        }
        var aux = next.buscarElem(valor)
        if (aux == -1) {
          return -1
        }
        return 1 + aux
      }
    }
  }

  def removerElemento(valor: Int): ListCircular = { //7
    var L: ListCircular = this
    var first: ListCircular = this
    var ant: ListCircular = null

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      while (L.last == false) {
        if (L.num == valor) {
          if (L == first) {
            ant = null
            first = L.next
            L.num = null
          } else {
            ant.next = L.next
            L.num = null
            L = ant
          }
        }
        ant = L
        L = L.next
      }
      if (L.num == valor) {
        ant.next = first
        ant.last = true
        L.num = null
      } else {
        if (L.next == null) {
          L.next = first
        }
      }

      return first
    }
  }

  def removerElementoRecursivo(valor: Int): ListCircular = { //8
    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      if (valor == num) {
        return this.next
      } else {
        if (next != null) {
          var aux = next.removerElementoRecursivo(valor)
          next = aux
          return this
        } else {
          return this
        }
      }
    }
  }

  def liberarLista(): ListCircular = { //9
    var l1: ListCircular = this
    var ant: ListCircular = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {

      while (l1.last == false) {
        l1.num = null
        //ant.ptr = l1.ptr
        l1 = l1.next
        //ant.ptr = null
      }
      l1.num = null
      l1.next = null
      return l1
    }
  }

  def printListCircular() {
    var L: ListCircular = this

    if (L.listaVazia() == 1) {
      printf("Lista Vazia")
    } else {
      println("Mostrando elementos: ")
      while (L.last == false) {
        println("Elemento: " + L.num)
        println("Proximo: " + L.next.num)
        println("Bool : " + L.last)
        println
        L = L.next
      }
      println("Elemento: " + L.num)
      println("Proximo: " + L.next.num)
      println("Bool : " + L.last)
      println

    }
  }

  override def toString() = {
    def ff(): String = {
      var s: String = ""
      var l: ListCircular = this

      while (l.last == false) {
        s += l.num
        s += " -> "
        l = l.next
      }
      s += l.num
      return s
    }
    ff()
  }

}