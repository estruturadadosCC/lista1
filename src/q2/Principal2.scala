package q2

object Principal2 extends App {
  
  var L = new ListaEncadeadaOrdenada;
  var L2 = new ListaEncadeadaOrdenada;

  print("Inserindo valores na lista...\n")

  L = L.inserirInicio(3)
  L = L.inserirInicio(5)
  L = L.inserirInicio(6)
  L = L.inserirInicio(7)
  L = L.inserirInicio(2)
  L = L.inserirInicio(30)
  
  print("Imprimindo lista...\n")
  L.imprimirLista()
  println

  print("Imprimindo lista usando recursão...\n")
  L.imprimirRecursivo()
  println

  print("Imprimindo lista em ordem reversa...\n")
  L.imprimirOrdemReversa()
  println

  print("Verificar se lista vazia:\n1-Lista Vazia 0-Lista Nao Vazia: \n")
  print(L.listaVazia())
  println
  
  print(L)
  println
  print("Remover o elemento 7 da lista. \n")
  L = L.removerElemento(7)
  print(L)
  println
  print("Remover o elemento 6 da lista. \n")
  L = L.removerElementoRecursivo(6)
  print(L)
  println
  
  print("Buscar o elemento 3 na lista...\n")
  print("O elemento 3 esta na posicao: " + L.buscarElem(3))
  println
  
  print("Recuperar o elemento que esta na posicao 2...\n")
  print("O elemento da posicao 2 eh: " + L.recuperarElem(2))
  println
  
  L2 = L2.inserirInicio(2)
  L2 = L2.inserirInicio(3)
  L2 = L2.inserirInicio(2)
  L2 = L2.inserirInicio(3)

  print("Imprimindo lista 1...\n")
  print(L)
  println
  print("Imprimindo lista 2...\n")
  print(L2)
  println
  print(L.verificarIgualdade(L2))
  println
    
  print("Esvaziar lista...\n")
  L = L.liberarLista()
  
  
  
  
}