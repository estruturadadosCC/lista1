package q3

class ListaDuplaEncadeada {
  //1
  var num: Integer = null
  var ant: ListaDuplaEncadeada = null
  var prox: ListaDuplaEncadeada = null

  def inserir(valor: Int): ListaDuplaEncadeada = { //2
    if (num == null) {
      num = valor
      return this
    } else {

      var l1: ListaDuplaEncadeada = this

      while (l1 != null) {
        if (valor < l1.num) {
          var L = new ListaDuplaEncadeada
          L.num = valor
          L.prox = l1
          l1.ant = L
          return L

        } else {
          if (valor >= l1.num && (l1.prox == null || valor <= l1.prox.num)) {
            var L2 = new ListaDuplaEncadeada
            L2.num = valor

            if (l1.prox != null) {
              l1.prox.ant = L2
            }
            L2.prox = l1.prox
            L2.ant = l1
            l1.prox = L2

            return this
          }
          l1 = l1.prox
        }
      }
      return this
    }
  }

  def imprimirLista() { //3
    var aux: ListaDuplaEncadeada = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      while (aux != null) {
        print(aux.num)
        aux = aux.prox
      }
    }
  }

  def imprimirRecursivo() { //4
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      print(num)
      if (prox != null) {
        prox.imprimirRecursivo()
      }
    }
  }

  def imprimirOrdemReversa() { //5
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      if (prox != null) {
        prox.imprimirOrdemReversa()
      }
      print(num)
    }
  }

  def listaVazia(): Int = { //6
    if (num == null) {
      return 1;
    }
    return 0;
  }

  def recuperarElem(indice: Int): Int = { //7.1
    var cont: Int = 0
    var aux: ListaDuplaEncadeada = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      while (aux != null) {
        if (cont == indice) {
          return aux.num
        }
        aux = aux.prox
        cont = cont + 1
      }
      return -1
    }
  }

  def buscarElem(valor: Int): Int = { //7.2
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      if (num == valor) {
        return 0
      } else {
        if (prox == null) {
          print("Nao Existe")
          return -1
        }
        var aux = prox.buscarElem(valor)
        if (aux == -1) {
          return -1
        }
        return 1 + aux
      }
    }
  }

  def removerElemento(valor: Int): ListaDuplaEncadeada = { //8
    var l1: ListaDuplaEncadeada = this
    var head: ListaDuplaEncadeada = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      while (l1 != null) {

        if (l1.num == valor) {

          if (l1.ant == null) {
            l1.num = null
            l1 = l1.prox
            l1.ant = null
            head = l1

          } else {
            l1.ant.prox = l1.prox
            if (l1.prox != null) {
              l1.prox.ant = l1.ant
            }
            l1.num = null
          }
        }
        l1 = l1.prox
      }
      return head
    }
  }
  
  def removerElementoRecursivo(valor: Int): ListaDuplaEncadeada = { //8
    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      if (valor == num) {
        return this.prox
      } else {
        if (prox != null) {
          var aux = prox.removerElementoRecursivo(valor)
          prox = aux
          return this
        } else {
          return this
        }
      }
    }
  }

  def liberarLista(): ListaDuplaEncadeada = { //10
    var l1: ListaDuplaEncadeada = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {

      while (l1.prox != null) {
        l1.num = null
        l1.ant = null
        l1 = l1.prox
      }
      l1.num = null
      return l1
    }
  }

  def verificarIgualdade(l: ListaDuplaEncadeada): Boolean = { //11
    var l1: ListaDuplaEncadeada = this
    var l2: ListaDuplaEncadeada = l

    if (l1.listaVazia() == 1 || l2.listaVazia() == 1) {
      printf("Lista Vazia")
      return false
    } else {
      while (l1 != null) {
        if (l1.num == l2.num) {
          l1 = l1.prox
          l2 = l2.prox

        } else {
          return false
        }

      }
    }

    return true
  }

  def printListaDupla() {
    var L: ListaDuplaEncadeada = this

    if (L.listaVazia() == 1) {
      printf("Lista Vazia")
    } else {
      while (L != null) {
        println("Elemento: " + L.num)
        if (L.ant != null) {
          println("Anterior : " + L.ant.num)
        } else {
          println("Anterior : " + L.ant)
        }

        if (L.prox != null) {
          println("Proximo: " + L.prox.num)
        } else {
          println("Proximo: " + L.prox)
        }

        println
        L = L.prox
      }
    }
  }

  override def toString() = {
    def ff(): String = {
      var s: String = ""
      var l: ListaDuplaEncadeada = this

      while (l.prox != null) {
        s += l.num
        s += " <-> "
        l = l.prox
      }
      s += l.num
      return s
    }
    ff()
  }

}