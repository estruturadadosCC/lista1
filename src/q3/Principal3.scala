package q3

object Principal3 extends App {
  
  var L = new ListaDuplaEncadeada;
  var L2 = new ListaDuplaEncadeada;

  print("Inserindo valores na lista...\n")

  L = L.inserir(3)
  L = L.inserir(1)
  L = L.inserir(2)
  L = L.inserir(4)
  L = L.inserir(6)
  
  print("Imprimindo lista...\n")
  L.imprimirLista()
  println

  print("Imprimindo lista usando recursão...\n")
  L.imprimirRecursivo()
  println

  print("Imprimindo lista em ordem reversa...\n")
  L.imprimirOrdemReversa()
  println

  print("Verificar se lista vazia:\n1-Lista Vazia 0-Lista Nao Vazia: \n")
  print(L.listaVazia())
  println
  
  print(L)
  println
  print("Remover o elemento 1 da lista. \n")
  L = L.removerElemento(1)
  print(L)
  println
  print("Remover o elemento 4 da lista. \n")
  L = L.removerElementoRecursivo(4)
  print(L)
  println

  
  print("Buscar o elemento 3 na lista...\n")
  print("O elemento 3 esta na posicao: " + L.buscarElem(3))
  println
  
  print("Recuperar o elemento que esta na posicao 2...\n")
  print("O elemento da posicao 2 eh: " + L.recuperarElem(2))
  println
  
  print("Esvaziar lista...\n")
  L = L.liberarLista()
  
  L = L.inserir(2)
  L = L.inserir(2)
  L = L.inserir(3)
  L = L.inserir(3)
  
  L2 = L2.inserir(2)
  L2 = L2.inserir(3)
  L2 = L2.inserir(2)
  L2 = L2.inserir(3)

  print("Imprimindo lista 1...\n")
  print(L)
  println
  print("Imprimindo lista 2...\n")
  print(L2)
  println
  print(L.verificarIgualdade(L2))
  println
  
  
  
}