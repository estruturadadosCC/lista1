package q1

object Principal1 extends App {

  var L = new ListaEncadeada[Integer]();

  print("Inserindo valores na lista...\n")

  L = L.inserirInicio(3)
  L = L.inserirInicio(5)
  L = L.inserirInicio(6)
  L = L.inserirInicio(8)
  L = L.inserirInicio(5)

  print("Imprimindo lista...\n")
  L.imprimirLista()
  println

  print("Imprimindo lista usando recursão...\n")
  L.imprimirRecursivo()
  println

  print("Imprimindo lista em ordem reversa...\n")
  L.imprimirOrdemReversa()
  println

  print("Verificar se lista vazia:\n1-Lista Vazia 0-Lista Nao Vazia: \n")
  print(L.listaVazia())
  println
  
  print(L)
  println
  print("Remover o elemento 8 da lista. \n")
  L = L.removerElemento(8)
  print(L)
  println
  print("Remover o elemento 6 da lista. \n")
  L = L.removerElementoRecursivo(6)
  print(L)
  println
  
  print("Liberar lista...\n")
  L.liberarLista()
  
  L = L.inserirInicio(2)
  L = L.inserirInicio(3)
  L = L.inserirInicio(1)
  L = L.inserirInicio(0)

  print("Imprimindo lista...\n")
  print(L)
  println
  
  print("Buscar o elemento 3 na lista...\n")
  print("O elemento 3 esta na posicao: " + L.buscarElem(3))
  println
  
  print("Recuperar o elemento que esta na posicao 2...\n")
  print("O elemento da posicao 2 eh: " + L.recuperarElem(2))
  println
  
  
  
  
}