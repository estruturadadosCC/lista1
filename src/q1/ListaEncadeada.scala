package q1

class ListaEncadeada[I >: Null]() {
  //1
  var num: I = null
  var ptr: ListaEncadeada[I] = null

  def inserirInicio(valor: I): ListaEncadeada[I] = { //2
    if (num == null) {
      num = valor
      return this
    } else {
      var L = new ListaEncadeada[I]
      L.num = valor
      L.ptr = this
      return L
    }
  }

  def imprimirLista() { //3
    var aux: ListaEncadeada[I] = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      while (aux != null) {
        print(aux.num)
        aux = aux.ptr
      }
    }
  }

  def imprimirRecursivo() { //4

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      print(num)
      if (ptr != null) {
        ptr.imprimirRecursivo()
      }
    }
  }

  def imprimirOrdemReversa() { //5
    if (this.listaVazia() == 1) {
      print("Lista Vazia")
    } else {
      if (ptr != null) {
        ptr.imprimirOrdemReversa()
      }
      print(num)
    }
  }

  def listaVazia(): Int = { //6
    if (num == null) {
      return 1;
    }
    return 0;
  }

  def recuperarElem(indice: Int): I = { //7.1
    var cont: Int = 0
    var aux: ListaEncadeada[I] = this

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      throw new IndexOutOfBoundsException
    } else {
      while (aux != null) {
        if (cont == indice) {
          return aux.num
        }
        aux = aux.ptr
        cont = cont + 1
      }
      print("Nao ha nenhum elemento com este indice")
      throw new IndexOutOfBoundsException
    }
  }

  def buscarElem(valor: I): Int = { //7.2
    var list: ListaEncadeada[I] = this
    var l1: ListaEncadeada[I] = new ListaEncadeada
    var cont: Int = 0

    if (this.listaVazia() == 1) {
      print("Lista Vazia")
      return -1
    } else {
      while (list != null) {
        if (list.num == valor) {
          return cont
        }
        cont = cont + 1
        list = list.ptr
      }
      return -1
    }
  }

  def removerElemento(valor: Int): ListaEncadeada[I] = { //8
    var l1: ListaEncadeada[I] = this
    var ant: ListaEncadeada[I] = null
    var prime: ListaEncadeada[I] = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      while (l1 != null) {
        if (l1.num == valor) {
          if (ant == null) {
            l1 = l1.ptr
            prime = l1
          } else {
            ant.ptr = l1.ptr
            l1 = ant
          }
        }
        ant = l1
        l1 = l1.ptr
      }
      return prime
    }
  }

  def removerElementoRecursivo(valor: Int): ListaEncadeada[I] = { //9

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {
      if (valor == num) {
        return this.ptr
      } else {
        if (ptr != null) {
          var aux = ptr.removerElementoRecursivo(valor)
          ptr = aux
          return this
        } else {
          return this
        }
      }
    }
  }

  def liberarLista(): ListaEncadeada[I] = { //10
    var l1: ListaEncadeada[I] = this
    var ant: ListaEncadeada[I] = this

    if (this.listaVazia() == 1) {
      printf("Lista Vazia")
      return this
    } else {

      while (l1 != null) {
        //l1.num = null
        ant.ptr = l1.ptr
        l1 = l1.ptr
        ant.ptr = null
      }
      return l1
    }
  }

  override def toString() = {
    def ff(): String = {
      var s: String = ""
      var l: ListaEncadeada[I] = this

      while (l.ptr != null) {
        s += l.num
        s += " -> "
        l = l.ptr
      }
      s += l.num
      return s
    }
    ff()
  }

}